package com.devcamp.task58a40.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.task58a40.models.Product;
import com.devcamp.task58a40.repository.ProductRepository;

@RestController
@CrossOrigin
public class ProductController {
    @Autowired
    ProductRepository productRepository;

    @GetMapping("/product")
    public ResponseEntity<List<Product>> getProduct(){
        try {
            List<Product> listProduct = new ArrayList<Product>();
            productRepository.findAll().forEach(listProduct::add);

            return new ResponseEntity<>(listProduct, HttpStatus.OK);
            
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }



    }

}
