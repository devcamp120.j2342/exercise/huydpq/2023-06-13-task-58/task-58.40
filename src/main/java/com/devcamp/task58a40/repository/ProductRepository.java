package com.devcamp.task58a40.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.task58a40.models.Product;

public interface ProductRepository extends JpaRepository<Product, Long> {
    
}
