package com.devcamp.task58a40;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Task58A40Application {

	public static void main(String[] args) {
		SpringApplication.run(Task58A40Application.class, args);
	}

}
